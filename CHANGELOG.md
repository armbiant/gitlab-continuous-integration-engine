# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.2.2] - 2023-02-01

-   No changelog

## [2.2.1] - 2023-01-30

-   No changelog

## [2.2.0] - 2023-01-25

-   No changelog

## [2.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [2.0.55] - 2023-01-18

-   No changelog

## [2.0.54] - 2023-01-16

-   No changelog

## [2.0.53] - 2023-01-10

-   No changelog

## [2.0.52] - 2023-01-04

### Changed

-   update golemio core due to dependency injection implementation

## [2.0.51] - 2022-12-13

-   No changelog

## [2.0.50] - 2022-12-09

-   No changelog

## [2.0.49] - 2022-12-07

-   No changelog

## [2.0.48] - 2022-11-29

-   No changelog

## [2.0.47] - 2022-11-14

-   No changelog

## [2.0.46] - 2022-11-03

-   No changelog

## [2.0.45] - 2022-10-20

### Changed

-   Update TypeScript to v4.7.2, ts-node-dev to v2.0.0

## [2.0.44] - 2022-10-11

-   No changelog

## [2.0.43] - 2022-09-21

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418)

### Removed

-   All references to the pid module ([pid#174](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/174))
-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))
